const express = require('express')
const router = express.Router()
const { check, validationResult } = require('express-validator')
const Contact = require('../models/contact')


router.get('/', async (req, res) => {
  const contacts = await Contact.find({}).exec()
  res.status(200).send({
    "status":"success",
    "contacts": contacts
  })
})

router.post('/', 
  [
    check('first_name').isLength({max: 15}).notEmpty(),
    check('last_name').isLength({max: 15}).notEmpty(),
    check('telephone').isLength({max: 15}).notEmpty(),
    check('email').isEmail().bail().notEmpty()
  ],
  async (req, res) => {
  const errors = await validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(401).send({
      "status": "failed",
      "desc": errors.array().map((error) => {
        return error.param +": " + error.msg }
      ).join(", ")
    })
  }

  const payload = req.body
  const contact = new Contact(payload)
  await contact.save().then(
    res.status(201).send({
      "status": "success",
      "desc": "created success."
    }).end()
  ).catch(
    res.status(401).send({
      "status": "failed",
      "desc": "cannot created because invalid some fields."
    })
  )
})

module.exports = router
