const mongoose = require('mongoose')
const Schema = mongoose.Schema

const contactSchema = new Schema(
  { 
    first_name: String,
    last_name: String,
    telephone:  String,
    email: String
  }
)

const ContactModel = mongoose.model('Contact', contactSchema)

module.exports = ContactModel