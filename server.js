const express = require('express')
var cors = require('cors')
const app = express()
var contactsRoutes = require('./routes/contacts')

const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost:27017/test-node-js', { useNewUrlParser: true })

app.use(cors())
app.use(express.json())
app.use('/contacts', contactsRoutes)

app.listen(3011, () => {
  console.log("listening port 3011")
})
